import styles from "./page.module.css";

const InterviewList = () => {
  // fetch data from API
  // TODO: change this hardcoded list
  const MOCK_LIST_DATA = [
    "OpenAI",
    "Coupang",
    "Meta",
    "Google",
    "Amazon",
    "Microsoft",
  ];
  // TODO: change key={item} to key={item.id}
  return (
    <ul className={styles.listContainer}>
      {MOCK_LIST_DATA.map((item) => (
        <li key={item}>{item}</li>
      ))}
    </ul>
  );
};

export default function DashboardLayout({
  children, // will be a page or nested layout
}: {
  children: React.ReactNode;
}) {
  return (
    <section>
      <nav className={styles.navBar}>
        <h1>Audio Replay</h1>
        <div className={styles.navBarSubContainer}>
          <span>Interviews</span>
          <span>Career</span>
          <span>Blogs</span>
          <span>Contact Us</span>
          <span>Pricing</span>
        </div>
      </nav>

      {/* three-columns layout */}
      <div className={styles.contentContainer}>
        <InterviewList />
        <div className={styles.detailContainer}>{children}</div>
      </div>
    </section>
  );
}
