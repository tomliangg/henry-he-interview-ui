import styles from "./page.module.css";

interface InterviewDetailProps {
  params: {
    id: string;
  };
}

export default function InterviewDetail({ params }: InterviewDetailProps) {
  // fetch interview detail using id
  const MOCK_DATA = [
    {
      id: "1",
      file: "url-to-audio-file",
      text: "Hello, thank you for joining us today",
      audioStartTime: "",
      audioEndTime: "",
    },
    {
      id: "2",
      file: "url-to-audio-file",
      text: "Let's start doing some codings",
      audioStartTime: "",
      audioEndTime: "",
    },
  ];

  const suggestionText = "this is a suggestion";

  return (
    <main className={styles.container}>
      <ul className={styles.transcriptContainer}>
        {MOCK_DATA.map((item) => (
          <li key={item.id}>
            <button>Play</button>
            <audio src=""></audio>
            <span>{item.text}</span>
          </li>
        ))}
      </ul>
      <div className={styles.suggestionContainer}>
        <span>{suggestionText}</span>
      </div>
    </main>
  );
}
